import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantService } from 'app/restaurants/restaurant.service';
import { Restaurant } from 'app/restaurants/restaurant/restaurant.model';

@Component({
  selector: 'mt-restaurant-details',
  templateUrl: './restaurant-details.component.html',
  styleUrls: ['./restaurant-details.component.css']
})
export class RestaurantDetailsComponent implements OnInit {

  restaurant: Restaurant;

  constructor(private service: RestaurantService, 
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.service.getDetailsRestaurant(this.route.snapshot.params['id'])
                .subscribe(restau => {
                  console.log(this.restaurant);
                  this.restaurant = restau;
                });

                
  }

}
