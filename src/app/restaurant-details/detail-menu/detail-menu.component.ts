import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantService } from 'app/restaurants/restaurant.service';
import { Observable } from 'rxjs/Observable';
import { MenuItem } from '../menu-item/menu-item.model';

@Component({
  selector: 'mt-detail-menu',
  templateUrl: './detail-menu.component.html',
  styleUrls: ['./detail-menu.component.css']
})
export class DetailMenuComponent implements OnInit {

  menu: Observable<MenuItem[]>
  constructor(private service: RestaurantService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.menu = this.service.getMenuOfRestaurant(this.route.parent.snapshot.params["id"]);    
  }

}
