import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantService } from 'app/restaurants/restaurant.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'mt-detail-reviews',
  templateUrl: './detail-reviews.component.html',
  styleUrls: ['./detail-reviews.component.css']
})
export class DetailReviewsComponent implements OnInit {

  reviews :  Observable<any>
  constructor(private service: RestaurantService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.reviews = this.service.getReviewsOfRestaurant(this.route.parent.snapshot.params["id"]);    

  }

}
