import { Observable } from 'rxjs/Observable';
import { Response } from "@angular/http";
import 'rxjs/add/observable/throw';
export class ErrorHandler
{
    static getErrorHandler(response : Response | any){

        let errorMessage: String;
        if(response instanceof Response)
        {
            errorMessage = `Error status ${response.status} ao acessar a url ${response.url} - ${response.statusText}`;           
        }
        else 
        {
            errorMessage = response.toString();
        }
        console.log(errorMessage)
        return Observable.throw(errorMessage);
    }
}