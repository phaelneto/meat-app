import { ErrorHandler } from './../app.error-handler';
import { API_URL } from './../../app.constants';
import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Restaurant } from "./restaurant/restaurant.model";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { MenuItem } from 'app/restaurant-details/menu-item/menu-item.model';

@Injectable()
export class RestaurantService
{
    constructor(private http: Http){
    }

    getRestaurants() : Observable<Restaurant[]>
    {
        return this.http.get(`${API_URL}/restaurants`)
                    .map(response => response.json())
                    .catch(ErrorHandler.getErrorHandler);     
    }

    getDetailsRestaurant(id: String) : Observable<Restaurant>
    {
        return this.http
                    .get(`${API_URL}/restaurants/${id}`)
                    .map(response => response.json())
                    .catch(ErrorHandler.getErrorHandler);     
    }


    getReviewsOfRestaurant(id: String) : Observable<any>
    {
        return this.http
                    .get(`${API_URL}/restaurants/${id}/reviews`)
                    .map(response => response.json())
                    .catch(ErrorHandler.getErrorHandler);     
    }

    getMenuOfRestaurant(id: String) : Observable<MenuItem[]>
    {
        return this.http
                    .get(`${API_URL}/restaurants/${id}/menu`)
                    .map(response => response.json())
                    .catch(ErrorHandler.getErrorHandler);     
    }
}