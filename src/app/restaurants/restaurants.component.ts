import { RestaurantService } from './restaurant.service';
import { Component, OnInit } from '@angular/core';
import { Restaurant } from './restaurant/restaurant.model';

@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.css']
})
export class RestaurantsComponent implements OnInit {

  restaurants : Restaurant[];
  
  constructor(private service : RestaurantService) { }

  ngOnInit() {
    this.service.getRestaurants().subscribe(restaurants => this.restaurants = restaurants);
  }

}
