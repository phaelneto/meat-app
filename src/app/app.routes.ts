import { RestaurantDetailsComponent } from './restaurant-details/restaurant-details.component';
import { RestaurantsComponent } from './restaurants/restaurants.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import {Routes} from "@angular/router"
import { DetailMenuComponent } from './restaurant-details/detail-menu/detail-menu.component';
import { DetailReviewsComponent } from './restaurant-details/detail-reviews/detail-reviews.component';

export const ROUTES: Routes = [
    {path: "", component: HomeComponent},
    {path: "restaurants", component: RestaurantsComponent},
    {path: "details/:id", component: RestaurantDetailsComponent,
        children: [
            {path: "", redirectTo: "menu", pathMatch:"full"},
            {path: "menu", component: DetailMenuComponent},
            {path: "reviews", component: DetailReviewsComponent} 
        ]
    },
    {path: "about", component: AboutComponent}


]